import React, { Component } from 'react';
import Navbar from './components/Navbar';
import Header from './components/Header';
import Section from './components/Section';
import List from './components/List';

import './App.scss';
class App extends Component {

  state = {
    list: ['Tomate', 'lechuga'],
    user: {
      title: 'Recipes4You es un página de recetas caseras, donde podrás encontrar gran variedad de recetas de todo el mundo. Con más de un millón de usuarios en los cinco contienentes, podrás experimental las recetas con un toque personal. Puedes además registrarte y incluir tus propias recetas!! A que estas esperando!',
      logo: 'https://www.pgerard.com/assets/images/welcome-logo-white720.png',
      image: 'https://bit.ly/3e7XhiJ',
      Recetas: [
        { title: 'Crema de calabaza', image: 'https://wl-genial.cf.tsp.li/resize/728x/jpg/51e/ae7/689f145b41abb18cc7e262e911.jpg', info: 'Pon 5 vasos de agua a cocer en una cazuela. Corta la rodaja de calabaza por la mitad, pélala y córtala primero en tiras y después en dados. Retira la parte inferior y superior de las zanahorias, pélalas y córtalas en rodajas de 1 centímetro. Limpia el puerro reserva un trozo y corta el resto en rodajas. Pela la patata y trocéala como para guisar. Introduce todas las verduras en la cazuela, añade 2 cucharadas de aceite y sal (1/2 cucharadita) a tu gusto. Pon la tapa y deja cocer durante 20 minutos a fuego medio. Tritura con la batidora eléctrica hasta que quede una crema homogénea. En el momento de servir, corta unas tiritas finas de puerro (reservado anteriormente) y espolvorea con ellas la superficie.' },
        { title: 'Salmón al horno', image: 'https://i.blogs.es/72963b/salmon-al-horno-con-patatas/1366_2000.jpg', info: 'Comenzamos preparando una cama sobre la que vamos a asar el salmón. Para ello, picamos las patatas en rodajas de medio cm de grosor, el pimiento y la cebolleta en juliana y el tomate en rodajas finas. Distribuimos todas las hortalizas sobre la bandeja del horno, sazonamos y añadimos un poco de aceite de oliva, un poco de agua y un poco de vino blanco. Horneamos a 190º durante 20 minutos hasta que las patatas estén tomando color y el resto de las hortalizas haya quedado bien pochado. En ese momento, espolvoreamos bien con pimienta negra y colocamos el lomo de salmón sazonado sobre la cama de hortalizas. Añadimos también unos aritos del verde de la cebolleta distribuyéndolos por encima. Dejamos que el salmón se cocine durante unos 10 minutos. Servimos directamente en la misma fuente de horno en la que lo hemos cocinado.' },
      ],
    },
  }

  addItem = (name, title, info) => {
    let newUser = this.state.user;

    newUser[name].push({ title, info });

    this.setState({ user: newUser })
  };

  addNote = (name, title, info) => {
    let newUser = this.state.user;

    newUser[name].push({ title, info });

    this.setState({ user: newUser })
  };
  render() {
    const { user, list } = this.state;
    return (
      <div className="App">
        <Navbar/>
        <Header
          mainTitle={user.title}
          mainLogo={user.logo}/>
        <Section recipe={user.Recetas} name={'Recetas'} addItem={this.addItem} />
        <List addNote={this.addNote} mainList={list}/>
      </div>
    );
  }
}
export default App;
