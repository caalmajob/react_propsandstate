import React, { Component } from 'react';
import './Section.scss'


class Section extends Component {
    state = {
        openForm: false,
        title: '',
        info: '',
    };
    //Abrir el formulario
    setOpenForm = () => {
        this.setState({ openForm: !this.state.openForm })
    };

    onInputChange = (ev) => {
        const { name, value } = ev.target;
        this.setState({ [name]: value });
    }

    submitForm = (ev) => {
        ev.preventDefault();

        const { title, info } = this.state;

        this.props.addItem(this.props.name, title, info)
        this.setOpenForm();
        this.setState({
            title: '',
            info: '',
        })
    }

    render() {
        return (
            <section className="section">
                <div className="section__container">
                    <div className="section__text">
                        <div className="Section__text-title">
                            <h1>{this.props.name}</h1>
                        </div>
                        <div className="section__text-recipe">
                            <ul>
                                {this.props.recipe.map((el, index) => {
                                    return (
                                        <li className="list" key={`${el.title}/${el.info}-${index}`}>
                                            <h3 className="list-title">{el.title}</h3>
                                            <p>{el.info}</p>
                                            <img className="list-img" src={el.image} />
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </div>

                    <div className="section__form">
                        {this.state.openForm && <div className="hide-form">
                            <form onSubmit={this.submitForm}>
                                <label htmlFor="title">Título: </label>
                                <input
                                    type="text"
                                    name="title"
                                    id="title"
                                    placeholder="Title"
                                    value={this.state.title}
                                    onChange={this.onInputChange} />
                                <label htmlFor="info">Pasos: </label>
                                <input
                                    type="text"
                                    name="info"
                                    id="info"
                                    placeholder="info"
                                    value={this.state.info}
                                    onChange={this.onInputChange} />
                                <button type="submit">Enviar!</button>
                            </form>
                        </div>}
                        <div className="button-form">
                            <button onClick={this.setOpenForm}>Añadir {this.props.name}</button></div>
                    </div>
                </div>
            </section>
        );
    }
}


export default Section;
