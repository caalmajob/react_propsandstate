import React, { Component } from 'react';
import './List.scss'


class List extends Component {

    render() {
        return (
            <div className="box">
                <div className="boxtitle">
                    <h1>Lista de la compra</h1>
                </div>
                <div className="box__list">
                    <ul className="box__ul">
                        {this.props.mainList.map((el, index) => {
                            return (
                                <li key={`${el}-${index}`}>
                                <input type="checkbox"></input> {el}
                                </li>
                            );
                        })}
                    </ul>
                </div>

            </div>
        )
    }
}

export default List;