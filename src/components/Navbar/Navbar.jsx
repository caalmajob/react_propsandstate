import React, { Component } from 'react';
import './Navbar.scss';

const navbarLinks = ['Inicio', 'Recetas', 'Registrate']

class Navbar extends Component {

    render() {
        return (
            <nav className="nav">
                <h1 className="main-text">Recipes4You</h1>
                <ul className="main-list">
                    {navbarLinks.map((item, index) => <li key={`${item}-${index}`}>{item}</li>
                    )}
                </ul>
            </nav>
        )
    }
}

export default Navbar;