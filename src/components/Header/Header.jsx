import userEvent from '@testing-library/user-event';
import React, { Component } from 'react';
import './Header.scss';

class Header extends Component {

    render() {
        console.log(this.props);
        return (
            <header className="header">
            <img className="header__image" src={this.props.mainLogo}/>
                <div className="header__container">
                    <div className="header__main">
                        <p>{this.props.mainTitle}</p>
                    </div>
                    {/* <h1>{this.props.fullName}</h1> */}
                </div>
            </header>
        )
    }
}

export default Header;